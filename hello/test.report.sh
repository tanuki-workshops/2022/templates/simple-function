#!/bin/bash
go get -u github.com/jstemmer/go-junit-report
GOOS=js GOARCH=wasm go test -exec="$(go env GOROOT)/misc/wasm/go_js_wasm_exec" -v 2>&1 | go-junit-report > report.xml

